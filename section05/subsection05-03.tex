\begin{definition}
    $\Gamma(p) = \int \limits_{0}^{+\infty} x^{p - 1} e^{-x} dx$ при $p > 0$~--- гамма-функция Эйлера.

    $B (p, q) = \int \limits_{0}^{1} x^{p - 1} (1 - x)^{q - 1} dx$ при $p, q > 0$~--- бета-функция Эйлера.
\end{definition}

\begin{properties} Гамма-функции:
    1. $\Gamma(p + 1) = p \Gamma(p)$.

    2. $\Gamma(n + 1) = n!$.

    3. $\Gamma(\frac{1}{2}) = \sqrt{\pi}$

    4. $\Gamma(n + \frac{1}{2}) = \frac{\sqrt{\pi} \cdot (2n - 1)!!}{2^n}$.

    5. $\Gamma \in C^{\infty}(0, +\infty)$ и $\Gamma^{(n)}(p) = \int \limits_{0}^{+\infty} x^{p - 1} \ln^n x \cdot e^{-x} dx$.

    6. $\Gamma$ строго выпукла.

    7. $\Gamma(p) \sim \frac{1}{p}$ при $p \to 0+$.
\end{properties}

\begin{proof}
    1. $\Gamma(p + 1) = \int \limits_{0}^{+\infty} x^{p} \cdot e^{-x} dx = $
    $x^p(-e^{-x})\bigr|_{x = 0}^{x = +\infty} + \int \limits_{0}^{+\infty} p x^{p - 1} e^{-x} dx = $
    $p \int \limits_{0}^{+\infty} x^{p - 1} e^{-x} dx = p \cdot \Gamma(p)$.

    2. Достаточно заметить, что $\Gamma(1) = 1$ и воспользоваться первым свойством.

    3. $\Gamma(\frac{1}{2}) = \int \limits_{0}^{+\infty} \frac{1}{\sqrt{x}} e^{-x} dx$.
    Сделаем замену $y = \sqrt{x}$, тогда $dy = \frac{1}{2} \frac{dx}{\sqrt{x}}$.
    $\int \limits_{0}^{+\infty} 2e^{-y^2} dy = \int \limits_{-\infty}^{+\infty} e^{-y^2} dy = \sqrt{\pi}$.

    4. Очевидно следует из 1 и 3.

    5. Нужно локальное условие Лебега в окрестности $p_0$.
    Возьмем $p \in [\alpha, \beta]$ и предъявим суммируемую мажоранту.
    $|x^{p - 1} \ln^n x \cdot e^{-x}| \le x^{\beta - 1} \ln^n x \cdot e^{-x}$ при $x \ge 1$.
    $|x^{p - 1} \ln^n x \cdot e^{-x}| \le x^{\alpha - 1} |\ln x|^n \cdot e^{-x} \le $
    $C \cdot x^{\alpha - 1 - \varepsilon}$ при $x < 1$.

    Так что мы можем просто дифференцировать функцию под интегралом. Несложно убедиться, что все сойдется.

    6. $\Gamma''(p) = \int \limits_{0}^{+\infty} x^{p - 1} \ln^2 x \cdot e^{-x} dx$,
    при этом подынтегральная функция больше нуля при $x \neq 1$, так что интеграл положителен,
    а это означает строгую выпуклость $\Gamma$.

    7. $p \cdot \Gamma(p) = \Gamma(p + 1) \to \Gamma(1) = 1$ при $p \to 0+$.

\end{proof}

Из этих свойств следует, что график гамма-функции выглядит следующим образом:

\includegraphics[width = \linewidth / 2]{section05/gammagraph.png}

\begin{properties} Бета-функции:

    1. $B(p, q) = B(q, p)$.

    2. $B(p, q) = \int \limits_{0}^{+\infty} \frac{t^{p - 1}}{(1 + t)^{p + q}} dt$
    
\end{properties}

\begin{proof}
    1. Надо в определении $B$-функции сделать замену $y = 1 - x$.

    2. $B(p, q) = \int \limits_{0}^{1} x^{p - 1} \cdot (1 - x)^{q - 1} dx$.
    Сделаем замену $x = \frac{t}{1 + t} = 1 - \frac{1}{1 + t}$, тогда $dx = \frac{dt}{(1 + t)^2}$.
    Получим: $\int \limits_{0}^{+\infty} (\frac{t}{1 + t})^{p - 1} \cdot (\frac{1}{1 + t})^{q - 1} \frac{dt}{(1 + t)^2} = $
    $\int \limits_{0}^{+\infty} \frac{t^{p - 1}}{(1 + t)^{p + q}} dt$.
\end{proof}

\begin{theorem}
    $B(p, q) = \frac{\Gamma(p) \cdot \Gamma(q)}{\Gamma(p + q)}$.
\end{theorem}

\begin{proof}
    $\Gamma(p) \cdot \Gamma(q) = \int \limits_{0}^{+\infty} x^{p - 1} e^{-x} \int \limits_{0}^{+\infty} y^{q - 1} e^{-y} dy dx$.
    Сделаем замену $u = y + x$:
    $\int \limits_{0}^{+\infty} x^{p - 1} \int \limits_{x}^{+\infty} (u - x)^{q - 1} e^{-u} du dx = $
    $\int \limits_{0}^{+\infty} \int \limits_{0}^{u} x^{p - 1} (u - x)^{q - 1} e^{-u} dx du$.
    Сделаем замену $v = \frac{x}{u}$:
    $\int \limits_{0}^{+\infty} \int \limits_{0}^{1} u^{p - 1} v^{p - 1} (1 - v)^{q - 1} u^{q - 1} u e^{-u} dv du = $
    $\int \limits_{0}^{+\infty} u^{p + q - 1} e^{-u} \int \limits_{0}^{1} v^{p - 1} (1 - v)^{q - 1} dv du = $
    $\int \limits_{0}^{+\infty} u^{p + q - 1} e^{-u} B(p, q) du = B(p, q) \Gamma(p + q)$.
\end{proof}

\begin{consequence} (Формула дополнения):
    
    $\Gamma(p) \Gamma(1 - p) = \frac{\pi}{\sin (p \pi)}$ при $0 < p < 1$.
\end{consequence}

\begin{proof}
    По теореме $\Gamma(p)  \Gamma(1 - p) = \Gamma(p + 1 - p) B(p, 1 - p) = B(p, 1 - p) = $
    $\int \limits_{0}^{+\infty} \frac{t^p}{1 + t} dt = \frac{\pi}{\sin (p \pi)}$ (без доказательства).
\end{proof}

\begin{consequence} (Формула удвоения):
    
    $\Gamma(p) \Gamma(p + \frac{1}{2}) = \frac{\sqrt{\pi}}{2^{2p - 1}} \Gamma(2p)$.
\end{consequence}

\begin{proof}
    $\sqrt{\pi} = \Gamma(\frac{1}{2})$, так что надо доказать, что
    $\frac{\Gamma(p) \Gamma(p)}{\Gamma(2p)} = \frac{\Gamma(\frac{1}{2})\Gamma(p)}{\Gamma(p + \frac{1}{2}) 2^{2p - 1}}$,
    что равносильно тому, что
    $B(p, p) = B(\frac{1}{2}, p) \cdot \frac{1}{2^{2p - 1}}$.
    $B(p, p) = \int \limits_{0}^{1} x^{p - 1} (1 - x)^{p - 1} dx = 2 \int \limits_{0}^{\frac{1}{2}} (x (1 - x))^{p - 1} dx$.
    Сделаем замену $x = \frac{1}{2} - y$:
    $2 \int \limits_{0}^{\frac{1}{2}} (\frac{1}{4} - y^2)^{p - 1} dy$.
    Сделаем замену $y^2 = \frac{t}{4}$:
    $2 \int \limits_{0}^{1} (\frac{1}{4})^{p - 1} (1 - t)^{p - 1} \frac{dt}{4 \sqrt{t}} = $
    $\frac{1}{2^{2p - 1}} \int \limits_{0}^{1} \frac{(1 - t)^{p - 1}}{\sqrt{t}} dt = $
    $\frac{B(\frac{1}{2}, p)}{2^{2p - 1}}$.
\end{proof}

\begin{theorem}
    $\Gamma(t + a) \sim t^a \Gamma(t)$ при $t \to +\infty$.
\end{theorem}

\begin{proof}
    Докажем для $t + 1$ ($(t + 1)^a \sim t^a$).
    $\frac{\Gamma(t + 1)}{\Gamma(t + a + 1)} \sim t^{-a}$.
    $\frac{\Gamma(t + 1) \Gamma(a)}{\Gamma(t + a + 1)} = B(a, t + 1) = $
    $\int \limits_{0}^{1} x^{a - 1} (1 - x)^t dx$.
    Сделаем замену $x = \frac{u}{t}$:
    $\int \limits_{0}^{t} \frac{u^{a - 1}}{t^{a - 1}} (1 - \frac{u}{t})^t \frac{du}{t} = $
    $\frac{1}{t^a} \int \limits_{0}^{t} u^{a - 1} (1 - \frac{u}{t})^t du$.
    Осталось показать, что $\int \limits_{0}^{t} u^{a - 1} (1 - \frac{u}{t})^t du \to \Gamma(a)$.
    $\lim \limits_{t \to +\infty} \int \limits_{0}^{+\infty} u^{a - 1} (1 - \frac{u}{t})^t \chi_{[0, t]}(u) du$.
    Перейдем к пределу под интегралом (потом объясним, почему так можно):
    $\int \limits_{0}^{+\infty} u^{a - 1} e^{-u} dx = \Gamma(a)$.
    Переходить к пределу можно, потому что $u^{a - 1} e^{-u}$~--- суммируемая мажоранта.
\end{proof}

\begin{theorem} (Формула Эйлера-Гаусса):
    
    $\Gamma(p) = \lim \limits_{n \to +\infty} \frac{n^{p} n!}{p(p + 1)\ldots(p + n)}$.
\end{theorem}

\begin{proof}
    $\Gamma(p + n) = (p + n - 1) \Gamma(p + n - 1) = \ldots = (p + n - 1)(p + n - 2) \ldots p \Gamma(p)$.
    $\frac{n^p n!}{p (p + 1) \ldots (p + n)} = \frac{n}{p + n} \frac{n^p (n - 1)! \Gamma(p)}{\Gamma(p + n)} \sim$
    $\frac{n^p (n - 1)! \Gamma(p)}{\Gamma(p + n)} = n^p \frac{\Gamma(n) \Gamma(p)}{\Gamma(n + p)} \sim $
    \graytext{/ по предыдущей теореме /}
    $\sim n^p \frac{\Gamma(n) \Gamma(p)}{n^p \Gamma(n)} = \Gamma(p)$.
\end{proof}

\begin{consequence}
    $p (1 + p) \ldots (n + p) \sim \frac{n! n^p}{\Gamma(p)}$.
\end{consequence}

\begin{observation}
    $p = \frac{1}{2}$:
    $\frac{1}{2} \cdot \frac{3}{2} \cdot \ldots \frac{2n + 1}{2} \sim \frac{n! \sqrt{n}}{\sqrt{\pi}}$.
    Перенесем факториал налево:
    $\frac{(2n + 1)!!}{2(2n)!!}  \sim \sqrt{\frac{n}{\pi}}$~--- формула Валлиса.
\end{observation}

\begin{example}
    1. $\int \limits_{0}^{+\infty} e^{-x^p} dx$ при $p > 0$.
    Сделаем замену $y = x^p$ ($dx = \frac{1}{p} y^{\frac{1}{p} - 1} dy$):
    $\int \limits_{0}^{+\infty} e^{-y} \frac{1}{p} y^{\frac{1}{p} - 1} dy = \frac{1}{p} \Gamma(\frac{1}{p}) = \Gamma(\frac{1}{p} + 1)$.

    2. $\int \limits_{0}^{\frac{\pi}{2}} \sin^{p - 1} x \cos^{q - 1} x dx = \frac{1}{2} B(\frac{p}{2}, \frac{q}{2})$.
    Сделаем замену $t = \sin^2 x$:
    $\int \limits_{0}^{1} t^{\frac{p - 2}{2}} (1 - t)^{\frac{q - 2}{2}} \frac{dt}{2} = \frac{1}{2} B(\frac{p}{2}, \frac{q}{2})$.
    В частности $\int \limits_{0}^{\frac{\pi}{2}} \sin^{p - 1} x dx = \int \limits_{0}^{\frac{\pi}{2}} \cos^{p - 1} x dx = $
    $\frac{1}{2} B(\frac{p}{2}, \frac{1}{2}) = \frac{1}{2} \frac{\Gamma(\frac{p}{2}) \sqrt{\pi}}{\Gamma(\frac{p + 1}{2})}$.

    3. $V_n(r)$~--- объем $n$-мерного шара радуса $r$. $V_n(r) = c_n r^n$, так что достаточно посчитать $c_n = V_n(1)$.
    По принципу Кавальери $V_n(1) = \int \limits_{-1}^{1} V_{n - 1}(\sqrt{1 - x^2}) dx = $
    $2 \int \limits_{0}^{1} c_{n - 1} (1 - x^2)^{\frac{n - 1}{2}} dx$.
    Сделаем замену $x = \sin \varphi$:
    $2 c_{n - 1} \int \limits_{0}^{\frac{\pi}{2}} \cos^n \varphi d \varphi = $
    \graytext{/ по предыдущему примеру /} 
    $ = 2 c_{n - 1} \frac{\Gamma(\frac{n + 1}{2}) \sqrt{\pi}}{2 \Gamma(\frac{n}{2} + 1)}$.
    То есть $c_n = c_{n - 1} \frac{\Gamma(\frac{n + 1}{2}) \sqrt{\pi}}{\Gamma(\frac{n}{2} + 1)} = $
    $c_{n - 2}  \frac{\Gamma(\frac{n + 1}{2}) \sqrt{\pi}}{\Gamma(\frac{n}{2} + 1)}$
    $\frac{\Gamma(\frac{n}{2}) \sqrt{\pi}}{\Gamma(\frac{n + 1}{2})} = \ldots = $
    $c_1 \frac{\sqrt{\pi}^{n - 1} \Gamma(\frac{3}{2})}{\Gamma(\frac{n}{2} + 1)} = $
    $2  \frac{\sqrt{\pi}^{n - 1} \frac{\sqrt{\pi}}{2}}{\Gamma(\frac{n}{2} + 1)} = $
    $\frac{\pi^{\frac{n}{2}}}{\Gamma(\frac{n}{2} + 1)}$.
\end{example}
