\begin{definition}
    Дифференциальная форма степени $k$~--- это
    $\omega = \sum \limits_{\#I = k} f_I dx_I$, где $I = \{i_1, i_2, \ldots, i_k\}$,
    $dx_I = dx_{i_1} \wedge dx_{i_2} \wedge \ldots \wedge dx_{i_k}$
    (внешнее произведение. Если переставить местами две дэшки, знак меняется).
\end{definition}

\begin{definition}
    Форма гладкая, если $f_I$~--- гладкие функции.
\end{definition}

\begin{example}
    1. $0$-формы~--- это функции.

    2. $1$-форма~--- это $f_1 dx_1 + f_2 dx_2 + \ldots + f_n d x_n$, в частности дифференциалы функций.

    3. $n$-форма в $\R^n$~--- это $f d x_1 \wedge dx_2 \wedge \ldots \wedge d x_n$.
\end{example}

\begin{definition}
    Внешнее произведение форм.

    $\omega = \sum \limits_{I} f_I dx_I$, $\lambda = \sum \limits_{J} g_J dx_J$.
    Тогда $\omega \wedge \lambda = \sum \limits_{I, J} f_I g_J dx_I \wedge dx_J$.
\end{definition}

\begin{example}
    $\R^3$. $\omega = x dy \wedge dz$, $\lambda = y dx + z dz$. 
    $\omega \wedge \lambda = (x dy \wedge dz) \wedge (y dx + z dz) = xy dy \wedge dz \wedge dx + xz dy \wedge dz \wedge dz = $
    $xy dy \wedge dz \wedge dx = xy dx \wedge dy \wedge dz$.
\end{example}

\begin{properties}
    1. Линейность.

    2. Если $\omega$~--- $k$-форма и $\lambda$~--- $l$-форма, то $\omega \wedge \lambda = (-1)^{kl} \lambda \wedge \omega$

    3. Если $\omega$~--- форма нечетной степени, то $\omega \wedge \omega = 0$.
\end{properties}

\begin{proof}
    1. Очевидно.

    2. Надо поменять их местами, для этого надо $kl$ свопов сделать.

    3. Следствие п.2.
\end{proof}

\begin{definition}

Что такое форма?

Форма $\omega : \R^n \times \R^n \times \ldots \times \R^n$ \graytext{/ k штук /} $\to \R$.

1. $\omega$ линейно по каждому из $k$ аргументов.

2. $\omega$ антисимметрично (при перестановке местами двух аргументов знак меняется).

Если $\omega$ зависит от аргумента, то для каждого аргумента это отдельная такая штука.

\end{definition}


\begin{statement}
    Размерность равна ${n \choose k}$.
\end{statement}

\begin{proof}
    Нужно задать $\omega$ на наборах из $k$ базисных векторов. Также надо смотреть только на наборы различных векторов
    (если поменять два одинаковых, знак должен поменяться),
    а также можно оставить только строго возрастающие наборы. Размерность получается ${n \choose k}$.

$dx_I$~--- это базисные элементы, а определение формы~--- это разложение по базису.
При этом $dx_i$~--- это такие штуки, что
$dx_{i_1} \wedge dx_{i_2} \wedge \ldots \wedge dx_{i_k}(e_{i_1}, e_{i_2}, \ldots, e_{i_k}) = 1$,
а на всех остальных наборах базисных векторов $= 0$.
\end{proof}

\begin{observation}
    $dx{i_1} \wedge \ldots dx_{i_k}(u_1, \ldots, u_k) = \Det 
    \begin{pmatrix}
        u_{1 i_1}& u_{1 i_2} & \ldots & u_{1 i_k}\\
        \vdots&\vdots&\ddots&\vdots\\
        u_{k i_1}& u_{k i_2} & \ldots & u_{k i_k}\\
    \end{pmatrix}$.
\end{observation}

\begin{observation}
    Подробнее про $1$-формы.
    $f_1 dx_1 + \ldots + f_n dx_n$~--- разложение по базису.
    $dx_i$~--- это проекция на ось $O_{x_i}$.
    Пусть $d F = f_1 dx_1 + \ldots + f_n dx_n$.
    Тогда $d F = F(x) + d_xF(h) + o(h)$ ($d_xF = (\frac{\partial F}{\partial x_1},  \ldots, \frac{\partial F}{\partial x_n})$).
\end{observation}

\begin{definition}
    Внешнее дифференцирование дифференциальной формы.
    Пусть $\omega = \sum f_I dx_I$.
    Тогда $d \omega := \sum df_I \wedge d x_I$.
\end{definition}

\begin{example}
    $\omega = P dx + Q dy$.
    $d \omega = dP \wedge dx + dQ \wedge dy = $
    $(\frac{\partial P}{\partial x} dx + \frac{\partial P}{\partial y} dy) \wedge dx + $
    $(\frac{\partial Q}{\partial x} dx + \frac{\partial Q}{\partial y} dy) \wedge dy = $
    $\frac{\partial P}{\partial y} dy \wedge dx + \frac{\partial Q}{\partial x} dx \wedge dy = $
    $(\frac{\partial Q}{\partial x} - \frac{\partial P}{\partial y}) dx \wedge dy$.
\end{example}

\begin{properties}
    1. Линейность. $d (a \omega + b \lambda) = a d \omega + b d \lambda$.

    2. $d(\omega \wedge \lambda) = d \omega \wedge \lambda + (-1)^k \omega \wedge d \lambda$, где $\omega$~--- $k$-форма.

    3. $d(d \omega) = 0$ (если коэффициенты $\omega$ из $C^2$).
\end{properties}

\begin{proof}
    1. $\omega = \sum f_I dx_I$, $\lambda = \sum g_I dx_I$.
    Тогда $d(a \omega + b \lambda) = \sum d(af_I + bg_I) dx_I = \sum (a df_I + b dg_I) dx_I = a d \omega + b d \lambda$.

    2. Из линейности можно считать, что $\omega = f dx_I$, $\lambda = g dx_J$.
    $d(\omega \wedge \lambda) = d(fg dx_I \wedge dx_J) = d(fg) \wedge dx_I \wedge dx_J = $
    $(f d g + g d f) \wedge dx_I \wedge dx_J = fdg \wedge dx_I \wedge dx_J + gdf \wedge dx_I \wedge dx_J$.
    При этом $d \omega \wedge \lambda = gdf \wedge dx_I \wedge dx_J$ и
    $\omega \wedge d \lambda = f dx_I \wedge dg \wedge dx_J$. Первое совпадает со вторым слагаемым,
    а второе отличается от первого слагаемого в $(-1)^k$ раз ($g$ степени $1$, $d x_I$ степени $k$).

    3. $d(d \omega) = d(d(f dx_I)) = d(df \wedge dx_I) = d(\sum \limits_{j} f'_{x_j} dx_j \wedge dx_I) = $
    $\sum \limits_{j} df'_{x_j} \wedge dx_j \wedge dx_I = $
    $\sum \limits_{j} \sum \limits_{i} f''_{x_j x_i} dx_i \wedge dx_j \wedge dx_I = 0$, так как если 
    $i = j$, то это просто ноль, а если $i \neq j$, то слагаемое $(i, j)$ противоположно слагаемому $(j, i)$.

\end{proof}

\begin{definition}
    Форма $\omega$~--- замкнутая, если $d \omega = 0$.
    
    Форма $\omega$~--- точная, если $\omega = d \lambda$ для некоторого $\lambda$.

    Форма $\omega$~--- локально точная, если у каждой точки найдется окрестность $U$, в которой есть такое $\lambda$, что
    $\omega = d \lambda$ в этой окрестности.
\end{definition}

\begin{theorem}
    Если коэффициенты $\omega$ из $C^1$, то из локальной точности следует замкнутость. 
\end{theorem}

\begin{proof}
    Возьмем точку $x$. У нее есть окрестность $U$, где живет $U$, т.ч. $d \lambda = \omega$ на $U$.
    $d \omega$ в этой окрестности равно $d(d \lambda) = 0$.
\end{proof}

\begin{observation}
    Точность $\Rightarrow$ локальная точность $\Rightarrow$ \graytext{/ если коэффициенты из $C^1$ /} $\Rightarrow$ замкнутость.
\end{observation}

\begin{example}
    $\omega = \frac{x dy - y dx}{x^2 + y^2}$ на $\R^2 \setminus \{(0, 0)\}$. $\omega$~--- замкнутая.
    $d \omega = d(\frac{x}{x^2 + y^2}) \wedge d y - d(\frac{y}{x^2 + y^2}) \wedge dx$.
    $d(\frac{x}{x^2 + y^2}) = \frac{dx (x^2 + y^2) - (2 x dx + 2 y dy) x}{(x^2 + y^2)^2} = $
    $\frac{y^2 dx - x^2 dx - 2xy dy}{(x^2 + y^2)^2}$. При этом $dy \wedge dy$ даст ноль.
    Аналогично со вторым слагаемым, получим в итоге
    $\frac{y^2 - x^2}{(x^2 + y^2)^2} dx \wedge dy - \frac{x^2 - y^2}{(x^2 + y^2)^2} dy \wedge dx = 0$.

    $\omega$ не точная. Форма степени один, так что точность~--- это наличие первообразной.

    $\int \limits_{\texttt{по единичной окружности}} \frac{x dy - y dx}{x^2 + y^2}$.
    Пусть $x = \cos t$, $y = \sin t$. Тогда $dx = -\sin t dt$, $dy = \cos t dt$.
    Получим $\int \limits_{0}^{2 \pi} \frac{\cos^2t + \sin^2 t}{\sin^2 t + \cos^2 t} dx = \int \limits_{0}^{2 \pi} 1 dt = 2 \pi \neq 0$
    $\Rightarrow $ нет первообразной, и форма не точна.
\end{example}

\begin{theorem}
    $\omega$~--- замкнутая форма в $\Omega \subset \R^n$. Тогда $\omega$~--- локально точная.
\end{theorem}

\begin{lemma} Пуанкаре:
    
    Если $\Omega$~--- выпуклая область в $\R^n$, и $\omega$~--- замкнутая форма в $\Omega$, то $\omega$~--- точная форма.
\end{lemma}

\begin{proof}
    Докажем только для $n = 2$.
    Для $n = 2$ бывают только $1$-формы и $2$-формы.

    Пусть $\omega$~--- $1$-форма. Надо доказать, что у нее есть первообразная. Как мы уже показывали,
    достаточно показать, что $\int \omega$ по любой простой замкнутой кривой равен нулю.
    $\omega = P dx + Q dy \Rightarrow \int \limits_{\gamma} \omega = $
    $\int \limits_{\texttt{по внутренности}} (\frac{\partial Q}{\partial x} - \frac{\partial P}{\partial y}) dx dy$ по формуле Грина.
    По замкнутости $\frac{\partial Q}{\partial x} - \frac{\partial P}{\partial y} = 0$.

    Пусть $\omega$~--- $2$-форма. $\omega = f dx \wedge dy$.
    $d(Q dy) = \frac{\partial Q}{\partial x} dx \wedge dy$, тогда достаточно взять в качестве $Q$ первообразную $f$ по $x$.
\end{proof}

\begin{observation}
    Мы пользуемся выпуклостью для того, чтобы если есть замкнутая кривая, то ее внутренность лежит внутри $\Omega$.
\end{observation}

\begin{proof} теоремы:
    
    Берем точку в $\Omega$, кружок, содержащий эту точку. Это выпуклое множество, можно применить лемму.
    Так для каждой точки, получаем локальную точность.
\end{proof}

\begin{definition}
    Перенос формы.

    $\omega = \sum f_I dy_I$~--- форма в $\Omega \subset \R^m$.
    $\varphi : \overline{\Omega} \to \Omega$~--- гладкая функция.

    $\varphi^* \omega := \sum (f_I \circ \varphi)(x) d \varphi_I(x)$ ($y_i = \varphi_i(x)$, $dy_i = d \varphi_i(x)$).

    \graytext{/ Дальше какое-то $\pm$ объяснение происходящего. Вот \href{https://youtu.be/QZ5ZyLWt0U4?list=PLodheWE7A8z-EWSoNbdsziuSo6hEGHuUD&t=1136}{ссылка с таймкодом}. Смотреть до 24:30 /} 
\end{definition}

\begin{properties} переноса:
    
    1. Линейность.

    2. $\varphi^* (f \cdot \omega) = (f \circ \varphi) \cdot \varphi^* \omega$.

    3. $\varphi^*(\omega \wedge \lambda) = \varphi^* \omega \wedge \varphi^* \lambda$.

    4. $\varphi^* (\psi^* \omega) = (\psi \circ \varphi)^* \omega$.

    5. $\varphi^*(d \omega) = d(\varphi^* \omega)$.
\end{properties}

\begin{proof}
    1. Формула линейна по форме, так что перенос тоже линеен.

    2. $\omega = \sum f_I dx_I$. $f \omega = \sum f f_I d x_I$. $\varphi^* \omega = \sum f_I \circ \varphi d\varphi_I$.
    $\varphi^*(f \omega) = \sum (f \circ \varphi) \cdot (f_I \circ \varphi) d \varphi_I$.

    3. Сумму можно выкинуть по линейности, коэффициент $f_I$ можно отбросить по свойству 2. Так что достаточно доказать для
    $\omega = dx_I = dx_{i_1} \wedge \ldots \wedge dx_{i_k}$, $\lambda = d x_{j_1} \wedge \ldots \wedge d x_{j_l}$.
    $\varphi^* \omega = d \varphi_{i_1} \wedge \ldots \wedge d \varphi_{i_k}$,
    $\varphi^* \lambda = d \varphi_{j_1} \wedge \ldots \wedge d \varphi_{j_l}$,
    $\varphi^* (\omega \wedge \lambda) = d \varphi_{i_1} \wedge \ldots \wedge d \varphi_{i_k} \wedge d \varphi_{j_1} \wedge \ldots$
    $\wedge d \varphi_{j_al}$.

    4. Опять же можно смотреть только за формой вида $d x_{i_1} \wedge \ldots \wedge d x_{i_k}$.

    $(\psi \circ \varphi)^* \omega = $
    $d x_{i_1} \wedge \ldots \wedge d x_{i_k} (\psi'(\varphi(x)) \varphi'(x) \xi_1, \ldots, \psi'(\varphi(x)) \varphi'(x) \xi_k)$
     по определению (взяли производную сложной функции).
     $\psi^* \omega = d x_{i_1} \wedge \ldots \wedge dx_{i_k} (\psi'(y) \xi_1, \ldots, \psi'(y) \xi_k) = $
     $d \psi_{i_1}(y) \wedge \ldots \wedge d \psi_{i_k}(y) (\xi_1, \ldots, \xi_k)$.
     Тогда $\varphi^* (\psi^* \omega) = $
     $d \psi_{i_1}(\varphi(x)) \wedge \ldots \wedge d \psi_{i_k}(\varphi(x)) (\varphi'(x) \xi_1, \ldots, \varphi'(x) \xi_k = $ 
    $d x_{i_1} \wedge \ldots \wedge d x_{i_k} (\psi'(\varphi(x)) \varphi'(x) \xi_1, \ldots, \psi'(\varphi(x)) \varphi'(x) \xi_k)$.

    5. По линейности достаточно доказать для $\omega = f d x_{i_1} \wedge \ldots \wedge d x_{i_k}$.
    $d \omega = d f \wedge d x_{i_1} \wedge \ldots \wedge d x_{i_k}$.
    $\varphi^* \omega = f \circ \varphi \cdot d \varphi_{i_1}(x) \wedge \ldots \wedge d \varphi_{i_k}(x)$.
    $\varphi^*(d \omega) = \varphi^*(\sum f'_{x_j} d x_j \wedge d x_{i_1} \wedge \ldots \wedge d x_{i_k}) = $
    $\sum f'_{x_j} \circ \varphi \cdot d \varphi_j \wedge d \varphi_{i_1} \wedge \ldots \wedge d \varphi_{i_k}$.
    $d (\varphi^* \omega) = d (f \circ \varphi \cdot d \varphi_{i_1} \wedge \ldots \wedge d \varphi_{i_k})$.
    Мы знаем, что $d (\omega \wedge \lambda) = d \omega \wedge \lambda + (-1)^? \omega \wedge d \lambda$.
    Если мы будем дифференцировать какой-нибудь из $d \varphi_{i_l}$, будет получаться ноль.
    Так что получим
    $d(f \circ \varphi) \wedge d \varphi_{i_1} \wedge \ldots \wedge d \varphi_{i_k}$.
    При этом $d (f \circ \varphi) = \sum (f \circ \varphi)'_{x_j} d x_j.$
    $(f \circ \varphi)'_{x_j} = (f(\varphi_1(x), \ldots, \varphi_k(x)))'_{x_j} = $
    $\sum (f'_{x_l} \circ \varphi) (\varphi_l)'_{x_j}$.
    Тогда $d(f \circ \varphi) = \sum \limits_{l} \sum \limits_{j} (f'_{x_l} \circ \varphi) (\varphi_l)'_{x_j} d x_j = $
    $\sum \limits_{l} f'_{x_l} \circ \varphi \cdot d \varphi_l$.

\end{proof}

\begin{example}
    $\varphi(x_1, x_2) = (x_1^2x_2, \ln (x_1 + x_2))$.

    $\varphi^*(d y_1) = d \varphi_1(x) = (\varphi_1)'_{x_1} dx_1 + (\varphi_1)'_{x_2} d x_2 = $
    $2 x_1 x_2 d x_1 + x_1^2 d x_2$.

    $\varphi^*(d y_2) = d \varphi_2(x) = (\varphi_2)'_{x_1} d x_2 + (\varphi_2)'_{x_2} d x_2 = $
    $\frac{dx_1 + dx_2}{x_1 + x_2}$.

    $\varphi^*(dy_1 \wedge dy_2) = $\graytext{/ по свойствам /}$ = \varphi^*(d y_1) \wedge \varphi^*(d y_2) = $
    $(2x_1 x_2 d x_1 + x_1^2 d x_2) \wedge \frac{1}{x_1 + x_2} (d x_1 + d x_2) = $
    $\frac{1}{x_1 + x_2}(2 x_1 x_2 d x_1 \wedge d x_2 + x_1^2 d x_2 \wedge d x_1) = $
    $\frac{2 x_1 x_2 - x_1^2}{x_1 + x_2} d x_1 \wedge d x_2$.
\end{example}
