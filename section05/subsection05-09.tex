\begin{theorem}
    Квадрат объема $k$-мерного косоугольного параллелепипеда в $\R^n$, натянутого на векторы $a_1, a_2, \ldots, a_k$ равен
    $\Det \begin{pmatrix}
        \langle a_1, a_1 \rangle & \ldots & \langle a_1, a_k \rangle\\
        \vdots & \ddots & \vdots\\
        \langle a_k, a_1 \rangle & \ldots & \langle a_k, a_k \rangle\\
    \end{pmatrix}$
\end{theorem}

\begin{proof}
    Пусть мы находимся в $\R^k$ и матрица $A := (a_1, a_2, \ldots, a_k)$,
    тогда $\operatorname{vol} = |\Det A|$.

    Пусть теперь мы находимся в $\R^n$ ($n > k$).
    Заметим, что $(\Det A)^2 = \Det A \cdot \Det A^T = \Det (A A^T)$, а это квадратная матрица в любом случае.
    $A A^T = (a_1, \ldots, a_k) \cdot
    \begin{pmatrix}
        a_1^T\\
        \vdots\\
        a_k^T\\
    \end{pmatrix} = $
    $\begin{pmatrix}
        \langle a_1, a_1 \rangle & \ldots & \langle a_1, a_k \rangle\\
        \vdots & \ddots & \vdots\\
        \langle a_k, a_1 \rangle & \ldots & \langle a_k, a_k \rangle\\
    \end{pmatrix}$.
    Ничего в этом рассуждении не зависит от базиса, так что это можно записать в $k$-мерном подпространстве в $\R^n$,
    в котором лежит наш параллелепипед. Для него мы получим квадрат объема, так что и в стандартном базисе $\R^n$
    будет квадрат объема. 
\end{proof}

\begin{definition} Форма объема.
    
    $\omega(\xi_1, \xi_2, \ldots, \xi_k) = \pm$
    $\sqrt{\Det \begin{pmatrix}
        \langle \xi_1, \xi_1 \rangle & \ldots & \langle \xi_1, \xi_k \rangle\\
        \vdots & \ddots & \vdots\\
        \langle \xi_k, \xi_1 \rangle & \ldots & \langle \xi_k, \xi_k \rangle\\
    \end{pmatrix}}$.

    Без знака не совсем форма, потому что если переставить местами аргументы, знак не поменяется, но все остальные свойства
    типа линейности выполняются. Так что давайте говорить, что знак плюс, если набор векторов согласован, и минус, если не согласован.
\end{definition}

\begin{definition}
    Интеграл фунции по элементарной поверхности.

    $\int \limits_{M} f d \lambda_k := \pm \int \limits_{M} f \omega$
    (при этом знак тот же самый, что и у $\omega$, чтобы они сократились и не мешали).

    \

    Запись через параметризацию.
    $\varphi : \Omega \to M$~--- параметризация элементарной поверхности.
    $\int \limits_{M} f d \lambda_k = \int \limits_{\Omega} f \circ \varphi$
    $\sqrt{\Det \left ( \left (\left \langle \frac{\partial \varphi}{\partial t_i},
    \frac{\partial \varphi}{\partial t_j} \right \rangle \right ) \Bigr|_{i, j = 1}^{k} \right )}$.
\end{definition}

\begin{properties}
    1. Линейность.

    2. Аддитивность.

    3. Не зависит от ориентации.

    4. Если $f \ge 0$, то $\int \limits_{M} f d \lambda_k \ge 0$.
\end{properties}

\begin{proof}
    1, 2. Следуют из этих же свойств для интеграла от формы.

    3. Следует из того, что мы выбираем знак так, чтобы убрать влияние ориентации.

    4. Следует из формулы для параметризации ($f \circ \varphi$ неотрицательно, корень из определителя неотрицателен).
\end{proof}

\begin{example} Двумерная поверхность в $\R^3$.
    
    $k = 2, n = 3$.

    $\begin{cases}
        x(u, v)\\
        y(u, v)\\
        z(u, v)
    \end{cases}$~--- параметризация.
    Вектор обозначим за $\overrightarrow{r}(u, v)$.
    Нас интересует матрица из скалярных произведений:
    $\begin{pmatrix}
        \langle r'_u, r'_u \rangle & \langle r'_u, r'_v \rangle\\
        \langle r'_v, r'_u \rangle & \langle r'_v, r'_v \rangle\\
    \end{pmatrix}$,
    при этом есть стандартные обозначения: $E := \langle r'_u, r'_u \rangle$,
    $F := \langle r'_u, r'_v \rangle$, $G := \langle r'_v, r'_v \rangle$.
    А весь этот набор называют \textbf{первой квадратичной формой поверхности}.
    $\Det \begin{pmatrix}
        E & F\\
        F & G\\
    \end{pmatrix} = EG - F^2 \ge 0$.

    \

    $\int \limits_{M} f d S \graytext{/ d S = d \lambda_2 /} = \int \limits_{\Omega} f(x(u, v), y(u, v), z(u, v)) \sqrt{EG - F^2} du dv$.

    $E = \langle r'_u, r'_u \rangle = (x'_u)^2 + (y'_u)^2 + (z'_u)^2$.

    $G = \langle r'_v, r'_v \rangle = (x'_v)^2 + (y'_v)^2 + (z'_v)^2$.

    $F = \langle r'_u, r'_v \rangle = x'_u x'_v + y'_u y'_v + z'_u z'_v$.

    \

    Физический смысл. Пусть фунция~--- плотность. Тогда проинтегрировав плотность, мы получим массу.
\end{example}

\begin{example}
    $\int \limits_{\texttt{по верхней половине сферы радуиса }R} z d S$.
    Надо параметризовать сферу:
    $x(u, v) = R \cos u \cos v$, $y(u, v) = R \sin u \cos v$, $z(u, v) = R \sin v$,
    где $0 \le u \le 2 \pi$, $0 \le v \le \frac{\pi}{2}$.
    То есть необходимо посчитать
    $\int \limits_{0}^{2 \pi} \int \limits_{0}^{\frac{\pi}{2}} R \sin v \sqrt{EG - F^2} d v d u$.

    $x'_u = -R \sin u \cos v$, $y'_u = R \cos u \cos v$, $z'_u = 0$.

    $x'_v = -R \cos u \sin v$, $y'_v = - \sin u \sin v$, $z'_v = R \cos v$.

    $E = (x'_u)^2 + (y'_u)^2 + (z'_u)^2 = R^2 \sin^2 u \cos^2 v + R^2 \cos^2 u \cos^2 v) = R^2 \cos^2 v$.

    $F = x'_u x'_v + y'_u y'_v + z'_u z'_v = R^2 \sin u \cos u \sin v \cos v - R^2 \sin u \cos u \sin v \cos v = 0$.

    $G = (x'_v)^2 + (y'_v)^2 + (z'_v)^2 = R^2 \cos^ u \sin^2 v + R^2 \sin^2 u \sin^2 v + R^2 \cos^2 v = $
    $R^2 \sin^2 v + R^2 \cos^2 v = R^2$.

    $\sqrt{EG - F^2} = \sqrt{R^4 \cos^2 v} = R^2 \cos v$.

    Посчитаем наш интеграл:
    $\int \limits_{0}^{2 \pi} \int \limits_{0}^{\frac{\pi}{2}} R \sin v R^3 \cos v d v d u = $
    $R^3 \int \limits_{0}^{2 \pi} \int \limits_{0}^{\frac{\pi}{2}} \cos v d v d u = $
    $2 \pi R^3 \int \limits_{0}^{\frac{\pi}{2}} \sin v \cos v d v = $

    $2 \pi R^3 \int \limits_{0}^{\frac{\pi}{2}} \sin v d \cos v = $
    $2 \pi R^3 \frac{\sin^2 v}{2} \bigr|_{0}^{\frac{\pi}{2}} = \pi R^3$.
\end{example}
